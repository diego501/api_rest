package com.mygroup.api.scrapping;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.Connection;
import org.jsoup.Connection.Response;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.mygroup.api.dto.AbcDto;
import com.mygroup.api.dto.AbcDto.ContentData;
import com.mygroup.api.dto.ImageData;
import com.mygroup.api.dto.Item;
import com.mygroup.api.exceptions.ApplicationException;
import com.mygroup.api.exceptions.InvalidParameterException;
import com.mygroup.api.exceptions.ItemsNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Component
public class WebScrapping {
	
	private static final Logger log = LoggerFactory.getLogger(WebScrapping.class);
	public static final String baseUrl = "https://www.abc.com.py/";

	public List<Item> getItemsList(String key, String obtenerImagen) {
		
		List<Item> items = new ArrayList<>();
		
		if(key == null || key.isEmpty()) {
			throw new InvalidParameterException();
		}
		
		try {									
			String searchUrl = baseUrl + "buscar/" + URLEncoder.encode(key, "UTF-8");
			
			// Compruebo si me da un 200 al hacer la petición
			if (getStatusConnectionCode(searchUrl) == 200) {
				
				// Obtengo el HTML del body de la pagina
				Element htmlBody = getHtmlBody(searchUrl);
				
				String stringData = htmlBody.getElementsByAttributeValue("type", "application/javascript").get(0).toString();			
				
				String jsonString = stringData.substring(stringData.indexOf("\"data\"")-1,stringData.indexOf(";Fusion.globalContentConfig"));
				
				Gson g = new Gson();
				AbcDto abcDto = g.fromJson(jsonString, AbcDto.class);
				
				if(abcDto.getData().size() > 0) {
					for(ContentData contentData : abcDto.getData()) {
						Item item = new Item();
						
						item.setFecha(contentData.getPublishDate());
						item.setEnlace(contentData.getWebsiteUrls());
						
						String enlaceFoto = contentData.getEnlaceFoto();
						item.setEnlaceFoto(enlaceFoto);
						
						if(enlaceFoto != null && obtenerImagen != null && !obtenerImagen.isEmpty() && "true".equals(obtenerImagen.toLowerCase())) {
							ImageData imageData = getImageContent(enlaceFoto);
							if(imageData != null) {
								item.setContenidoFoto(imageData.getContentBase64());
								item.setContentTypeFoto(imageData.getContentType());
							}							
						}						
						
						item.setTitulo(contentData.getHeadlines().getBasic());
						item.setResumen(contentData.getSubheadlines().getBasic());
						
						items.add(item);
					}
				}else {
					throw new ItemsNotFoundException(key);
				}
				
			}else {				
				log.error("El Status Code de la pagina no es OK es: "+getStatusConnectionCode(searchUrl));
				throw new ApplicationException();
			}
			
		} catch (JsonSyntaxException jse) {
			log.error("Excepción en el formato del json de respuesta: " + jse.getMessage());
			throw new ApplicationException();
		} catch (UnsupportedEncodingException ue) {
			log.error("Excepción al codificar el parametro de la url: " + ue.getMessage());
			throw new ApplicationException();
		}catch(Exception e) {
			log.error("Error inesperado: " + e.getMessage());
			throw new ApplicationException();
		}
		
		return items;
	}
	
	private static int getStatusConnectionCode(String url) {		
		Response response = null;		
		try {
			response = Jsoup.connect(url).userAgent("Mozilla/5.0").timeout(100000).ignoreHttpErrors(true).execute();
		} catch (IOException ex) {
			log.error("Excepción al obtener el Status Code de la pagina: " + ex.getMessage());
			throw new ApplicationException();
		}
		return response.statusCode();
	}
	
	private static Element getHtmlBody(String url) {
		Document doc = null;
		try {
			doc = Jsoup.connect(url).userAgent("Mozilla/5.0").timeout(100000).get();			
		} catch (IOException ex) {			
			log.error("Error al obtener el HTML de la página" + ex.getMessage());
			throw new ApplicationException();
		}
		return doc.body();
	}
	
	private ImageData getImageContent(String urlImage) {
		Connection.Response res = null;
		ImageData image = null;
		try {
			res = Jsoup.connect(urlImage).ignoreContentType(true).method(Connection.Method.GET).execute();		
			
			String imageBase64 = new String(Base64.getEncoder().encode(res.bodyAsBytes()), "UTF-8");
			image = new ImageData(imageBase64, res.contentType());
		}catch (UnsupportedEncodingException ue) {
			log.error("Error al codificar la imagen a base64");			
		} catch (IOException ex) {			
			log.error("Error al obtener la imagen de la url: " + urlImage);			
		}
						
		return image;
	}
	
}
