package com.mygroup.api.restcontroller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.mygroup.api.dto.Item;
import com.mygroup.api.scrapping.WebScrapping;

@RestController
public class AbcRestController {

	@Autowired
	private WebScrapping webScrapping;
	
	@GetMapping("consulta")
	public List<Item> getItemsList(@RequestParam("q") String key, @RequestParam(name = "f", required = false) String obtenerFoto) {
		return webScrapping.getItemsList(key, obtenerFoto);
	}
}
