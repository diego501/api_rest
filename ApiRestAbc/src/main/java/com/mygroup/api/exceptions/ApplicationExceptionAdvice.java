package com.mygroup.api.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.mygroup.api.dto.ErrorDto;

@ControllerAdvice
public class ApplicationExceptionAdvice {

	@ResponseBody
	  @ExceptionHandler(ApplicationException.class)
	  @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	  ErrorDto applicationExceptionHandler(ApplicationException ex) {
	    return new ErrorDto("g100", ex.getMessage());
	  }
}
