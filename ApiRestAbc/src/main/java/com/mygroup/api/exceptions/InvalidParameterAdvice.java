package com.mygroup.api.exceptions;



import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.mygroup.api.dto.ErrorDto;

@ControllerAdvice
public class InvalidParameterAdvice {

	@ResponseBody
	  @ExceptionHandler(InvalidParameterException.class)
	  @ResponseStatus(HttpStatus.BAD_REQUEST)
	  ErrorDto invalidParameterHandler(InvalidParameterException ex) {
	    return new ErrorDto("g268", ex.getMessage());
	  }
}
