package com.mygroup.api.exceptions;

public class ItemsNotFoundException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ItemsNotFoundException(String key) {
	    super("No se encuentran noticias para el texto: " + key);
	  }
}
