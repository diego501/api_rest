package com.mygroup.api.exceptions;

public class InvalidParameterException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public InvalidParameterException() {
	    super("Parámetros inválidos");
	  }
}
