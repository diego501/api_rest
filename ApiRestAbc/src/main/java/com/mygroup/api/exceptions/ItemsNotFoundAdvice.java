package com.mygroup.api.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.mygroup.api.dto.ErrorDto;

@ControllerAdvice
public class ItemsNotFoundAdvice {

	@ResponseBody
	  @ExceptionHandler(ItemsNotFoundException.class)
	  @ResponseStatus(HttpStatus.NOT_FOUND)
	  ErrorDto itemsNotFoundHandler(ItemsNotFoundException ex) {
	    return new ErrorDto("g267", ex.getMessage());
	  }
}
