package com.mygroup.api.exceptions;

public class ApplicationException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ApplicationException() {
	    super("Error interno del servidor");
	  }
}
