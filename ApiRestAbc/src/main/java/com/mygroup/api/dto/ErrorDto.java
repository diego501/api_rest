package com.mygroup.api.dto;

import java.io.Serializable;

public class ErrorDto implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String codigo;
	
	private String error;
	
	public ErrorDto(String codigo, String error) {
		this.codigo = codigo;
		this.error = error;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

}
