package com.mygroup.api.dto;


import java.util.List;

import org.json.simple.JSONObject;

import com.google.gson.annotations.SerializedName;

public class AbcDto {

	@SerializedName("data")
	private List<ContentData> data;
	
	@SerializedName("metadata")
	private JSONObject metadata;
	
	public List<ContentData> getData() {
		return data;
	}

	public void setData(List<ContentData> data) {
		this.data = data;
	}

	public JSONObject getMetadata() {
		return metadata;
	}

	public void setMetadata(JSONObject metadata) {
		this.metadata = metadata;
	}

	public class ContentData{
		
		@SerializedName("_id")
		private String id;
		
		@SerializedName("_website_urls")
		private List<String> websiteUrls;

		@SerializedName("description")
		private Basic description;
		
		@SerializedName("display_date")
		private String displayDate;
		
		@SerializedName("headlines")
		private Basic headlines;
		
		@SerializedName("promo_items")
		private PromoItems promoItems;
		
		@SerializedName("publish_date")
		private String publishDate;
		
		@SerializedName("subheadlines")
		private Basic subheadlines;
		
		@SerializedName("taxonomy")
		private Taxonomy taxonomy;

		public String getId() {
			return id;
		}

		public void setId(String id) {
			this.id = id;
		}

		public String getWebsiteUrls() {
			return this.websiteUrls != null && this.websiteUrls.size() > 0 ? this.websiteUrls.get(0) : "";
		}

		public void setWebsiteUrls(List<String> websiteUrls) {
			this.websiteUrls = websiteUrls;
		}

		public Basic getDescription() {
			return description;
		}

		public void setDescription(Basic description) {
			this.description = description;
		}

		public String getDisplayDate() {
			return displayDate;
		}

		public void setDisplayDate(String displayDate) {
			this.displayDate = displayDate;
		}

		public Basic getHeadlines() {
			return headlines;
		}

		public void setHeadlines(Basic headlines) {
			this.headlines = headlines;
		}

		public PromoItems getPromoItems() {
			return promoItems;
		}
		
		public String getEnlaceFoto() {			
			if(this.getPromoItems() != null && this.getPromoItems().getBasic() != null) {				
				return this.getPromoItems().getBasic().getUrl();				
			}
			return null;
		}

		public void setPromoItems(PromoItems promoItems) {
			this.promoItems = promoItems;
		}

		public String getPublishDate() {
			return publishDate;
		}

		public void setPublishDate(String publishDate) {
			this.publishDate = publishDate;
		}

		public Basic getSubheadlines() {
			return subheadlines;
		}

		public void setSubheadlines(Basic subheadlines) {
			this.subheadlines = subheadlines;
		}

		public Taxonomy getTaxonomy() {
			return taxonomy;
		}

		public void setTaxonomy(Taxonomy taxonomy) {
			this.taxonomy = taxonomy;
		}
		
	}
	
	public class PromoItems{
		@SerializedName("basic")
		private BasicImage basic;

		public BasicImage getBasic() {
			return basic;
		}

		public void setBasic(BasicImage basic) {
			this.basic = basic;
		}
		
	}
	
	public class Taxonomy{
		
		@SerializedName("primary_section")
		private TaxonomyData primarySection;

		public TaxonomyData getPrimarySection() {
			return primarySection;
		}

		public void setPrimarySection(TaxonomyData primarySection) {
			this.primarySection = primarySection;
		}
		
	}
	
	public class TaxonomyData{
		
		@SerializedName("_id")
		private String id;
		
		@SerializedName("name")
		private String name;

		public String getId() {
			return id;
		}

		public void setId(String id) {
			this.id = id;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}
		
	}
	
	public class Basic{
		
		@SerializedName("basic")
		private String basic;

		public String getBasic() {
			return basic;
		}

		public void setBasic(String basic) {
			this.basic = basic;
		}
		
	}
	
	public class BasicImage{
		
		@SerializedName("resized_urls")
		private BasicImageResizedUrl resizedUrls;

		@SerializedName("type")
		private String type;
		
		@SerializedName("url")
		private String url;

		public BasicImageResizedUrl getResizedUrls() {
			return resizedUrls;
		}

		public void setResizedUrls(BasicImageResizedUrl resizedUrls) {
			this.resizedUrls = resizedUrls;
		}

		public String getType() {
			return type;
		}

		public void setType(String type) {
			this.type = type;
		}

		public String getUrl() {
			return url;
		}

		public void setUrl(String url) {
			this.url = url;
		}
		
	}
	
	public class BasicImageResizedUrl{
		
		@SerializedName("270x175")
		private String image270x175;
		
		@SerializedName("910x590")
		private String image910x590;

		public String getImage270x175() {
			return image270x175;
		}

		public void setImage270x175(String image270x175) {
			this.image270x175 = image270x175;
		}

		public String getImage910x590() {
			return image910x590;
		}

		public void setImage910x590(String image910x590) {
			this.image910x590 = image910x590;
		}
		
	}
}

