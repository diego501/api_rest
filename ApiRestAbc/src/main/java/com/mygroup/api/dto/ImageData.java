package com.mygroup.api.dto;

import java.io.Serializable;

public class ImageData implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String contentBase64;
	
	private String contentType;
	
	public ImageData(String contentBase64, String contentType) {
		this.contentBase64 = contentBase64;
		this.contentType = contentType;
	}

	public String getContentBase64() {
		return contentBase64;
	}

	public void setContentBase64(String contentBase64) {
		this.contentBase64 = contentBase64;
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	} 

}